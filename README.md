## Welcome!
Hi! This is an simple HideAndSeek game made in Unreal Engine version 5.2.

## Start Info
-   The AssetList is documented at "Content/Thirdparty/DT_ThirdPartyDataTable"
-   "BP_Default_HideAndSeek" is the main settings file used for changing things like Enemy Count, Time, etc. but some things can also be set in the "BP_Default_Player", "BP_Default_Seeker" and "BP_Default_SeekerAIController"

## Game Overview
-   GameMode Checks for Spawns marked as Hider/Seeker -Spawns and saves them
-   GameMode Spawns Player at random Hider-Spawn-Locaton
-   GameMode Start HideTimer, that gets displayed in UI of the player
-   HideTimer triggers "SpawnSeeker" at finish and start SeekerTimer
-   Seekers get an random walkable point on the map and walk there, if they found the spot, they rotate looking for player, then find another point on the map
-   If Seeker see player, they run to player and if they touch him, triggers "PlayerLost"
-   SeekerTimer triggers "PlayerWin" and player has won
-   The Win and the Lose Screen both give an option to quit the game or play another round

## Code

-   Functions and Variables where ordered in this format:
    -   CONSTRUCTOR
    -   PRIVATE VARIABLES
    -   PROTECTED VARIABLES
    -   PUBLIC VARIABLES
    -   PRIVATE FUNCTIONS
    -   PROTECTED FUNCTIONS
    -   PUBLIC FUNCTIONS
-   "auto" was used when the type was clear by name, or was irrelevant
-   if(auto CLASS == GetClass()) was often used to ensure validation of variables
-   if(auto CLASS == GetClass()) was often used in combination with UE_LOG(LogXYZ, Error, TEXT("ERROR - Where? - What?"))
