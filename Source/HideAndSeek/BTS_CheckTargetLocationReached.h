// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTS_CheckTargetLocationReached.generated.h"

/**
 * 
 */
UCLASS()
class HIDEANDSEEK_API UBTS_CheckTargetLocationReached : public UBTService
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector TargetLocationReachedKey;
	
	UPROPERTY(EditAnywhere)
	float AcceptanceRadius = -1.0f;

private:
	bool GetTargetLocationReached(UBehaviorTreeComponent& OwnerComp);
	void UpdateBehavior(UBehaviorTreeComponent& OwnerComp);
protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
