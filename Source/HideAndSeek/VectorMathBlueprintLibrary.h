// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "VectorMathBlueprintLibrary.generated.h"

USTRUCT(BlueprintType)
struct FMeshData_VMBL
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	TArray<FVector> Vertices;

	UPROPERTY(BlueprintReadWrite)
	TArray<int> Triangles;
};

/**
 * 
 */
UCLASS()
class HIDEANDSEEK_API UVectorMathBlueprintLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	static FVector2D RotateAround2DOrigin(const FVector2D& Point2D, float Degree = 90.0f, bool Clockwise = true);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	static FMeshData_VMBL CreateCircleSlice(float Degree, float Radius, int TriangleCount = 16);
};
