//Copyright 2023 Martin/Hana Zander All rights reserved.

#include "Player_Character.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "HideAndSeek_GameModeBase.h"
#include "Kismet/GameplayStatics.h"

APlayer_Character::APlayer_Character()
{
	PrimaryActorTick.bCanEverTick = true;
}

void APlayer_Character::OnMovementInput(const FInputActionInstance& Instance)
{
	FVector2D MovementVector2D = Instance.GetValue().Get<FVector2D>();
	AddMovementInput(GetActorForwardVector() * MovementVector2D.X);
	AddMovementInput(GetActorRightVector() * MovementVector2D.Y);
}

void APlayer_Character::OnLookInput(const FInputActionInstance& Instance)
{
	FVector2D LookVector2D = Instance.GetValue().Get<FVector2D>();
	AddControllerYawInput(LookVector2D.X);
}

void APlayer_Character::Die()
{
	auto HideAndSeekGameMode = Cast<AHideAndSeek_GameModeBase>(GetWorld()->GetAuthGameMode());
	HideAndSeekGameMode->EndGame(false);
}

FGenericTeamId APlayer_Character::GetGenericTeamId() const
{
	return FGenericTeamId(1);
}

void APlayer_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent);
	EnhancedInputComponent->BindAction(MovementInputAction, ETriggerEvent::Triggered, this, &APlayer_Character::OnMovementInput);
	EnhancedInputComponent->BindAction(LookInputAction, ETriggerEvent::Triggered, this, &APlayer_Character::OnLookInput);
	
	if (auto PlayerController = UGameplayStatics::GetPlayerController(this, 0))
	{
		if (auto EnhancedInputLocalPlayerSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			EnhancedInputLocalPlayerSubsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

