// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "BTT_StartMoveTo.h"

#include "AIController.h"
#include "AI/NavigationSystemBase.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Navigation/PathFollowingComponent.h"

EBTNodeResult::Type UBTT_StartMoveTo::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (auto AIController = OwnerComp.GetAIOwner())
	{
		if (auto AIPawn = AIController->GetPawn())
		{
			if (auto BlackboardComponent = OwnerComp.GetBlackboardComponent())
			{
				FAIMoveRequest MoveRequest;
				MoveRequest.SetAcceptanceRadius(AcceptanceRadius);
		
				if (auto TargetActor = Cast<AActor>(BlackboardComponent->GetValueAsObject(TargetLocationKey.SelectedKeyName)))
				{
					MoveRequest.SetGoalActor(TargetActor);
				}
				else
				{
					FVector TargetLocation = BlackboardComponent->GetValueAsVector(TargetLocationKey.SelectedKeyName);
					MoveRequest.SetGoalLocation(TargetLocation);
				}
		
				AIController->MoveTo(MoveRequest);
				return EBTNodeResult::Succeeded;
			}
		}
	}

	UE_LOG(LogBehaviorTree, Error, TEXT("ERROR | UBTT_StartMoveTo::ExecuteTask | NULL Reference"));
	return EBTNodeResult::Failed;
}
