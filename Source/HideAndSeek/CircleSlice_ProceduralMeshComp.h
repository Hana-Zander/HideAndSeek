// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "CircleSlice_ProceduralMeshComp.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class HIDEANDSEEK_API UCircleSlice_ProceduralMeshComp : public UProceduralMeshComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Degree = 90.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Radius = 100.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int TriangleCount = 16;

public:
	UFUNCTION(BlueprintCallable)
	void CreateMesh();
};
