//Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "EnhancedInputSubsystemInterface.h"
#include "GenericTeamAgentInterface.h"
#include "InputAction.h"
#include "GameFramework/Character.h"
#include "Player_Character.generated.h"

UCLASS()
class HIDEANDSEEK_API APlayer_Character : public ACharacter, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	APlayer_Character();

public:
	UPROPERTY(EditDefaultsOnly)
	UInputMappingContext* DefaultMappingContext;
	
	UPROPERTY(EditDefaultsOnly)
	UInputAction* MovementInputAction;

	UPROPERTY(EditDefaultsOnly)
	UInputAction* LookInputAction;

private:
	void OnMovementInput(const FInputActionInstance& Instance);
	void OnLookInput(const FInputActionInstance& Instance);

public:
	void Die();
	virtual FGenericTeamId GetGenericTeamId() const override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
