// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "Hider_PlayerController.h"

void AHider_PlayerController::BeginPlay()
{
	Super::BeginPlay();
	FInputModeGameOnly IputModeData;
	SetInputMode(IputModeData);
}
