// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_RotateAround.generated.h"

/**
 * 
 */
UCLASS()
class HIDEANDSEEK_API UBTT_RotateAround : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	bool StopRotation = false;

	UPROPERTY(EditAnywhere)
	float RotationSpeed = 1.0f;
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
