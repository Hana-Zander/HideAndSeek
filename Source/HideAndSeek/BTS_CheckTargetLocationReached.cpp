// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "BTS_CheckTargetLocationReached.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"

bool UBTS_CheckTargetLocationReached::GetTargetLocationReached(UBehaviorTreeComponent& OwnerComp)
{
	if (auto BlackBoardComponent = OwnerComp.GetBlackboardComponent())
	{
		if (auto AIOwner = OwnerComp.GetAIOwner())
		{
			if (auto AIPawn = AIOwner->GetPawn())
			{
				FVector PawnLocation = AIPawn->GetActorLocation();
				FVector TargetLocation;
				if (auto TargetPlayer = Cast<AActor>(BlackBoardComponent->GetValueAsObject("TargetPlayer")))
				{
					TargetLocation = TargetPlayer->GetActorLocation();
				}
				else
				{
					TargetLocation = BlackBoardComponent->GetValueAsVector("TargetLocation");
				}
				
				float DistanceToTargetLocation = FVector::Distance(TargetLocation, PawnLocation);
				return (DistanceToTargetLocation <= AcceptanceRadius);
			}
		}
	}
	UE_LOG(LogBehaviorTree, Error, TEXT("ERROR | UBTS_CheckTargetLocationReached::GetTargetLocationReached | NULL Reference"));
	return false;
}

void UBTS_CheckTargetLocationReached::UpdateBehavior(UBehaviorTreeComponent& OwnerComp)
{
	bool NewTargetLocationReached = GetTargetLocationReached(OwnerComp);
	if (auto BlackBoardComponent = OwnerComp.GetBlackboardComponent())
	{
		if (auto AIOwner = OwnerComp.GetAIOwner())
		{
			if (NewTargetLocationReached)
			{
				AIOwner->StopMovement();
			}
			BlackBoardComponent->SetValueAsBool(TargetLocationReachedKey.SelectedKeyName, NewTargetLocationReached);
			return;
		}
	}
	UE_LOG(LogBehaviorTree, Error, TEXT("ERROR | UBTS_CheckTargetLocationReached::UpdateBehavior | NULL Reference"));
}

void UBTS_CheckTargetLocationReached::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	UpdateBehavior(OwnerComp);
}
