// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "BTT_GetNewTargetLocation.h"

#include "AIController.h"
#include "NavigationSystem.h"
#include "AI/NavigationSystemBase.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UBTT_GetNewTargetLocation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto NavSystem = FNavigationSystem::GetCurrent<UNavigationSystemV1>(this);

	if (auto AIOwner = OwnerComp.GetAIOwner())
	{
		if (auto AIPawn = AIOwner->GetPawn())
		{
			FVector OriginPawnLocation = AIPawn->GetActorLocation();
			FNavLocation NewNavLocation;
			NavSystem->GetRandomPointInNavigableRadius(OriginPawnLocation, Radius, NewNavLocation);
			OwnerComp.GetBlackboardComponent()->SetValueAsVector(TargetLocationKey.SelectedKeyName, NewNavLocation.Location);
			return EBTNodeResult::Succeeded;
		}
	}
	
	UE_LOG(LogNavigation, Error, TEXT("ERROR | GetNewTargetLocation::ExecuteTask | NULL Reference"));
	return EBTNodeResult::Failed;
}
