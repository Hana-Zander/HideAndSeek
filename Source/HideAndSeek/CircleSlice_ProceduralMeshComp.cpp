// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "CircleSlice_ProceduralMeshComp.h"
#include "VectorMathBlueprintLibrary.h"

void UCircleSlice_ProceduralMeshComp::CreateMesh()
{
	auto MeshData = UVectorMathBlueprintLibrary::CreateCircleSlice(Degree, Radius, TriangleCount);
	TArray<FVector> Verticies = MeshData.Vertices;
	TArray<int> Triangles = MeshData.Triangles;
	CreateMeshSection(0, Verticies, Triangles, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>(), false);
}
