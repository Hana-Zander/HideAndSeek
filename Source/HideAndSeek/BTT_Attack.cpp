// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "BTT_Attack.h"

#include "Player_Character.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UBTT_Attack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (auto BlackBoardComponent = OwnerComp.GetBlackboardComponent())
	{
		if (auto PlayerCharacter = Cast<APlayer_Character>(BlackBoardComponent->GetValueAsObject(TargetPlayerKey.SelectedKeyName)))
		{
			PlayerCharacter->Die();
			return EBTNodeResult::Succeeded;
		}
	}
	
	UE_LOG(LogBehaviorTree, Error, TEXT("ERROR | UBTT_Attack::ExecuteTask | Couldn't Find An Usable APlayer_Character To Attack"));
	return EBTNodeResult::Failed;
}
