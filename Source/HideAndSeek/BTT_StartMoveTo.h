// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_StartMoveTo.generated.h"

/**
 * 
 */
UCLASS()
class HIDEANDSEEK_API UBTT_StartMoveTo : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector TargetLocationKey;
	
	UPROPERTY(EditAnywhere)
	float AcceptanceRadius = -1.0f;
	
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
