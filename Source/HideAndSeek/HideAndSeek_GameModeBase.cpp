// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "HideAndSeek_GameModeBase.h"

#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "Hider_PlayerController.h"
#include "Hider_HUD.h"

void AHideAndSeek_GameModeBase::SetSpawnArrays()
{
	TArray<AActor*> SpawnActors;
	UGameplayStatics::GetAllActorsOfClass(this, APlayerStart::StaticClass(), SpawnActors);

	for (auto SpawnActor : SpawnActors)
	{
		APlayerStart* Spawn = Cast<APlayerStart>(SpawnActor);
		if (Spawn->PlayerStartTag == HiderSpawnTag)
		{
			HiderSpawnArray.Add(Spawn);
			continue;
		}
		if (Spawn->PlayerStartTag == SeekerSpawnTag)
		{
			SeekerSpawnArray.Add(Spawn);
			continue;
		}
	}
}

APlayerStart* AHideAndSeek_GameModeBase::GetRandomSpawn(TArray<APlayerStart*>& SpawnArray)
{
	if (SpawnArray.Num() == 0) return 0;
	
	int RandomIndex = FMath::RandRange(0, SpawnArray.Num() - 1);
	return SpawnArray[RandomIndex];
}

void AHideAndSeek_GameModeBase::StartHideTimer()
{
	OnHideTimerStart();
	GetWorld()->GetTimerManager().SetTimer(HideTimerHandle, this, &AHideAndSeek_GameModeBase::StartSeekTimer, HideTime, false);
}

void AHideAndSeek_GameModeBase::StartSeekTimer()
{
	OnSeekTimerStart();
	GetWorld()->GetTimerManager().SetTimer(SeekTimerHandle, this, &AHideAndSeek_GameModeBase::OnSeekTimerEnd, SeekTime, false);
}

float AHideAndSeek_GameModeBase::GetTimerTimeLeft(const FTimerHandle& TimerHandle)
{
	if (TimerHandle.IsValid())
	{
		return GetWorld()->GetTimerManager().GetTimerRemaining(TimerHandle);
	}
	return -1.0f;
}

void AHideAndSeek_GameModeBase::SpawnSeeker()
{
	if (SeekerCharacterClass.Get())
	{
		for (int i = 0; i < SeekerAmount; i++)
		{
			auto RandomSpawn = GetRandomSpawn(SeekerSpawnArray);
			if (RandomSpawn)
			{
				auto RandomSpawnLocation = RandomSpawn->GetActorLocation();
				FRotator RandomSpawnRotation = FRotator(0,FMath::RandRange(0, 360),0);
				FActorSpawnParameters SpawnParameters;
				SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
				GetWorld()->SpawnActor(SeekerCharacterClass, &RandomSpawnLocation, &RandomSpawnRotation, SpawnParameters);
			}
			else
			{
				
				UE_LOG(LogSpawn, Error, TEXT("ERROR | AHideAndSeek_GameModeBase::SpawnSeeker | NO SPAWN FOUND FOR SEEKER TO SPAWN"));
			}
		}
	}
	else
	{
		UE_LOG(LogClass, Error, TEXT("ERROR | AHideAndSeek_GameModeBase::SpawnSeeker | NO CHARACTER-CLASS FOUND FOR SEEKER TO SPAWN"));
	}
}


void AHideAndSeek_GameModeBase::OnHideTimerStart()
{
	HiderHUD->DisplayHideTimer(true);
}

void AHideAndSeek_GameModeBase::OnSeekTimerStart()
{
	HiderHUD->DisplayHideTimer(false);
	HiderHUD->DisplaySeekTimer(true);
	SpawnSeeker();
}

void AHideAndSeek_GameModeBase::OnSeekTimerEnd()
{
	HiderHUD->DisplaySeekTimer(false);
	EndGame(true);
}

void AHideAndSeek_GameModeBase::OnPlayerWin()
{
	HiderHUD->DisplaySeekTimer(false);
	HiderHUD->DisplayWinScreen(true);
	UGameplayStatics::SetGamePaused(this, true);

	HiderController->SetInputMode(FInputModeUIOnly());
	HiderController->SetShowMouseCursor(true);
}

void AHideAndSeek_GameModeBase::OnPlayerLost()
{
	HiderHUD->DisplaySeekTimer(false);
	HiderHUD->DisplayLoseScreen(true);
	UGameplayStatics::SetGamePaused(this, true);

	HiderController->SetInputMode(FInputModeUIOnly());
	HiderController->SetShowMouseCursor(true);
}

void AHideAndSeek_GameModeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GetWorld()->GetTimerManager().ClearTimer(HideTimerHandle);
	HideTimerHandle.Invalidate();

	GetWorld()->GetTimerManager().ClearTimer(SeekTimerHandle);
	SeekTimerHandle.Invalidate();
	Super::EndPlay(EndPlayReason);
}

void AHideAndSeek_GameModeBase::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	SetSpawnArrays();
}

AActor* AHideAndSeek_GameModeBase::FindPlayerStart_Implementation(AController* Player, const FString& IncomingName)
{
	return GetRandomSpawn(HiderSpawnArray);
}

void AHideAndSeek_GameModeBase::StartPlay()
{
	Super::StartPlay();
	HiderController = Cast<AHider_PlayerController>(UGameplayStatics::GetPlayerController(this, 0));
	HiderHUD = Cast<AHider_HUD>(HiderController->GetHUD());
	HiderHUD->Init(HiderController);
	StartHideTimer();
}

float AHideAndSeek_GameModeBase::GetHideTimeLeft()
{
	return GetTimerTimeLeft(HideTimerHandle);
}

float AHideAndSeek_GameModeBase::GetHideTimeLeftPercent()
{
	return (GetHideTimeLeft()/HideTime);
}

float AHideAndSeek_GameModeBase::GetSeekTimeLeft()
{
	return GetTimerTimeLeft(SeekTimerHandle);
}

float AHideAndSeek_GameModeBase::GetSeekTimeLeftPercent()
{
	return (GetSeekTimeLeft()/SeekTime);
}

void AHideAndSeek_GameModeBase::EndGame(bool PlayerWon)
{
	if (PlayerWon)
	{
		OnPlayerWin();
		return;
	}

	OnPlayerLost();
}

void AHideAndSeek_GameModeBase::RestartGame()
{
	FName LevelName = FName(GetWorld()->GetName());
	UE_LOG(LogTemp, Error, TEXT("%s"), *LevelName.ToString())
	UGameplayStatics::OpenLevel(this, LevelName);
}
