// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "BTT_RotateAround.h"

#include "AIController.h"
#include "Seeker_Character.h"

EBTNodeResult::Type UBTT_RotateAround::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (auto AIOwner = OwnerComp.GetAIOwner())
	{
		if (auto SeekerCharacter = Cast<ASeeker_Character>(AIOwner->GetPawn()))
		{
			if (StopRotation)
			{
				SeekerCharacter->RotateAround(0);
				return EBTNodeResult::Succeeded;
			}
	
			SeekerCharacter->RotateAround(RotationSpeed);
			return EBTNodeResult::Succeeded;
		}
	}
	
	UE_LOG(LogBehaviorTree, Error, TEXT("ERROR | UBTT_RotateAround::ExecuteTask | Couldn't Find An Usable ASeeker_Character As Owner"));
	return EBTNodeResult::Failed;
}
