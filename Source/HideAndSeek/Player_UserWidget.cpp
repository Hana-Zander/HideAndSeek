// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "Player_UserWidget.h"

void UPlayer_UserWidget::Show_Implementation()
{
	SetVisibility(ESlateVisibility::SelfHitTestInvisible);
}

void UPlayer_UserWidget::Update_Implementation()
{
}

void UPlayer_UserWidget::Hide_Implementation()
{
	SetVisibility(ESlateVisibility::Hidden);
}
