// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_SetBoolValue.generated.h"

/**
 * 
 */
UCLASS()
class HIDEANDSEEK_API UBTT_SetBoolValue : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector BoolKey;
	
	UPROPERTY(EditAnywhere)
	bool NewValue = false;
	
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
