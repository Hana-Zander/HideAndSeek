// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "CircleSlice_ProceduralMeshComp.h"
#include "GameFramework/Character.h"
#include "GameFramework/RotatingMovementComponent.h"
#include "Seeker_Character.generated.h"

class UBlackboardComponent;

UCLASS()
class HIDEANDSEEK_API ASeeker_Character : public ACharacter
{
	GENERATED_BODY()

public:
	ASeeker_Character();

private:
	float WalkSpeedCache = -1.0f;
	float StuckTime = 0.0f;
	float MaxStuckTime = 2.0f;

	UPROPERTY()
	UBlackboardComponent* BlackboardComponent = 0;
	
protected:
	FRotator RotateAroundDirection = FRotator(0,1,0);
	
public:
	UPROPERTY(EditDefaultsOnly)
	URotatingMovementComponent* RotatingMovementComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UCircleSlice_ProceduralMeshComp* SightSenseVisualization;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* SightSenseVisualizationMaterial = 0;
	
	UPROPERTY(EditDefaultsOnly)
	float RunSpeedMultiplier = 1.5f;
	
public:
	void RotateAround(float RotationSpeed = 1.0f);
	void StartRunning();
	void StopRunning();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	void UnStuck();
};
