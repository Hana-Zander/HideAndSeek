// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Seeker_AIController.generated.h"

struct FAIStimulus;
/**
 * 
 */
UCLASS()
class HIDEANDSEEK_API ASeeker_AIController : public AAIController
{
	GENERATED_BODY()

public:
	ASeeker_AIController();

private:
	FTimerHandle ForgetTargetPlayerHandle;
	
public:
	UPROPERTY(EditDefaultsOnly)
	UBehaviorTree* BehaviorTree;

	UPROPERTY(EditDefaultsOnly)
	float RotateAroundTime = 5.0f;

	//How long does the Seeker remember the player, after it lost vision to the player
	UPROPERTY(EditDefaultsOnly)
	float RememberPlayerTime = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAIPerceptionComponent* AIPerceptionComponent;

protected:
	virtual void BeginPlay() override;
	virtual bool InitializeBlackboard(UBlackboardComponent& BlackboardComp, UBlackboardData& BlackboardAsset) override;
	virtual void OnTargetPlayerSensed();
	virtual void OnTargetPlayerForgotten();
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	virtual void Tick(float DeltaTime) override;
	virtual FGenericTeamId GetGenericTeamId() const override;

	UFUNCTION()
	void OnPerceptionChanged(AActor* Actor, FAIStimulus Stimulus);
};
