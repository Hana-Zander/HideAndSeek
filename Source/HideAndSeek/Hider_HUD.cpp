// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "Hider_HUD.h"
#include "Hider_PlayerController.h"
#include "Kismet/KismetSystemLibrary.h"

void TryDisplayPlayerUserWidget(UPlayer_UserWidget* Widget, bool ShouldDisplay)
{
	if (ShouldDisplay)
	{
		if(Widget) Widget->Show();
		return;
	}

	if(Widget) Widget->Hide();
}

void CreatePlayerUserWidget(UPlayer_UserWidget*& OutWidget, TSubclassOf<UPlayer_UserWidget> WidgetClass, AHider_PlayerController* OwnerController)
{
	if (WidgetClass && OwnerController)
	{
		OutWidget = Cast<UPlayer_UserWidget>(CreateWidget(OwnerController, WidgetClass));
		OutWidget->AddToViewport();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("ERROR | CreatePlayerUserWidget | NULL REFERENCE"));
		UKismetSystemLibrary::QuitGame(OwnerController, OwnerController, EQuitPreference::Quit, false);
	}
}

void AHider_HUD::Init(AHider_PlayerController* NewHiderPlayerController)
{
	this->HiderPlayerController = NewHiderPlayerController;
	
	CreatePlayerUserWidget(SeekTimerWidget, SeekTimerWidgetClass, HiderPlayerController);
	CreatePlayerUserWidget(HideTimerWidget, HideTimerWidgetClass, HiderPlayerController);
	CreatePlayerUserWidget(WinScreenWidget, WinScreenWidgetClass, HiderPlayerController);
	CreatePlayerUserWidget(LoseScreenWidget, LoseScreenWidgetClass, HiderPlayerController);
}

void AHider_HUD::DisplayHideTimer(bool ShouldDisplay)
{
	TryDisplayPlayerUserWidget(HideTimerWidget, ShouldDisplay);
}

void AHider_HUD::DisplaySeekTimer(bool ShouldDisplay)
{
	TryDisplayPlayerUserWidget(SeekTimerWidget, ShouldDisplay);
}

void AHider_HUD::DisplayWinScreen(bool ShouldDisplay)
{
	TryDisplayPlayerUserWidget(WinScreenWidget, ShouldDisplay);
}

void AHider_HUD::DisplayLoseScreen(bool ShouldDisplay)
{
	TryDisplayPlayerUserWidget(LoseScreenWidget, ShouldDisplay);
}
