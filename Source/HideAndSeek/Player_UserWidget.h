// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Player_UserWidget.generated.h"

/**
 * 
 */
UCLASS()
class HIDEANDSEEK_API UPlayer_UserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent)
	void Show();

	UFUNCTION(BlueprintNativeEvent)
	void Update();

	UFUNCTION(BlueprintNativeEvent)
	void Hide();
};
