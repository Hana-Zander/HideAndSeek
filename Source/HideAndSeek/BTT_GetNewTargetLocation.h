// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_GetNewTargetLocation.generated.h"

/**
 * 
 */
UCLASS()
class HIDEANDSEEK_API UBTT_GetNewTargetLocation : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector TargetLocationKey;

	UPROPERTY(EditAnywhere)
	float Radius = 1000.0f;
	
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
