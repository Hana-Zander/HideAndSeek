// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "VectorMathBlueprintLibrary.h"
#include "Kismet/KismetMathLibrary.h"

FVector2D UVectorMathBlueprintLibrary::RotateAround2DOrigin(const FVector2D& Point2D, float Degree, bool Clockwise)
{
	FVector2D NewPoint2D;
	float NewPointDegree = Degree;

	if (Clockwise)
	{
		NewPointDegree = 360 - Degree;
	}
	
	NewPoint2D.X = Point2D.X * UKismetMathLibrary::DegCos(NewPointDegree) - Point2D.Y * UKismetMathLibrary::DegSin(NewPointDegree);
	NewPoint2D.Y = Point2D.Y * UKismetMathLibrary::DegCos(NewPointDegree) + Point2D.X * UKismetMathLibrary::DegSin(NewPointDegree);
	return NewPoint2D;
}

FMeshData_VMBL UVectorMathBlueprintLibrary::CreateCircleSlice(float Degree, float Radius, int TriangleCount)
{
	FMeshData_VMBL SliceCircleMeshData;
	FVector2D StartPoint = FVector2D();
	
	//Add Origin Point To Vertices
	SliceCircleMeshData.Vertices.Add(FVector(0));
	
	//Create Right Side
	StartPoint = FVector2D(0, Radius);
	for (int i = 0; i < TriangleCount/2; i++)
	{
		FVector2D NewPoint = RotateAround2DOrigin(StartPoint, Degree/TriangleCount);

		SliceCircleMeshData.Triangles.Add(0);

		SliceCircleMeshData.Triangles.Add(SliceCircleMeshData.Vertices.Num());
		SliceCircleMeshData.Vertices.Add(FVector(StartPoint.X, StartPoint.Y, 0));
		
		SliceCircleMeshData.Triangles.Add(SliceCircleMeshData.Vertices.Num());
		SliceCircleMeshData.Vertices.Add(FVector(NewPoint.X, NewPoint.Y, 0));
		
		StartPoint = NewPoint;
	}

	//Create Left Side
	StartPoint = FVector2D(0, Radius);
	for (int i = 0; i < TriangleCount/2; i++)
	{
		FVector2D NewPoint = RotateAround2DOrigin(StartPoint, Degree/TriangleCount,false);

		SliceCircleMeshData.Triangles.Add(0);

		SliceCircleMeshData.Triangles.Add(SliceCircleMeshData.Vertices.Num());
		SliceCircleMeshData.Vertices.Add(FVector(NewPoint.X, NewPoint.Y, 0));
		
		SliceCircleMeshData.Triangles.Add(SliceCircleMeshData.Vertices.Num());
		SliceCircleMeshData.Vertices.Add(FVector(StartPoint.X, StartPoint.Y, 0));
		
		StartPoint = NewPoint;
	}

	return SliceCircleMeshData;
}
