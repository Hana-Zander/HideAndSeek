// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Player_UserWidget.h"
#include "GameFramework/HUD.h"
#include "Hider_HUD.generated.h"

class AHider_PlayerController;
/**
 * 
 */
UCLASS()
class HIDEANDSEEK_API AHider_HUD : public AHUD
{
	GENERATED_BODY()

public:
	UPROPERTY()
	AHider_PlayerController* HiderPlayerController = 0;
	
	//HideTimerWidget
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UPlayer_UserWidget> HideTimerWidgetClass = 0;
	
	UPROPERTY(BlueprintReadOnly)
	UPlayer_UserWidget* HideTimerWidget = 0;

	//SeekTimerWidget
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UPlayer_UserWidget> SeekTimerWidgetClass = 0;
	
	UPROPERTY(BlueprintReadOnly)
	UPlayer_UserWidget* SeekTimerWidget = 0;

	//WinScreenWidget
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UPlayer_UserWidget> WinScreenWidgetClass = 0;
	
	UPROPERTY(BlueprintReadOnly)
	UPlayer_UserWidget* WinScreenWidget = 0;

	//LoseScreenWidget
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UPlayer_UserWidget> LoseScreenWidgetClass = 0;
	
	UPROPERTY(BlueprintReadOnly)
	UPlayer_UserWidget* LoseScreenWidget = 0;

public:
	void Init(AHider_PlayerController* NewHiderPlayerController);
	
	void DisplayHideTimer(bool ShouldDisplay);
	void DisplaySeekTimer(bool ShouldDisplay);
	void DisplayWinScreen(bool ShouldDisplay);
	void DisplayLoseScreen(bool ShouldDisplay);
};
