// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "Seeker_Character.h"

#include "Seeker_AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AIPerceptionSystem.h"
#include "Perception/AISenseConfig_Sight.h"

ASeeker_Character::ASeeker_Character()
{
	PrimaryActorTick.bCanEverTick = true;
	RotatingMovementComponent = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("RotatingMovementComponent"));  
	SightSenseVisualization = CreateDefaultSubobject<UCircleSlice_ProceduralMeshComp>(TEXT("SightSenseVisualization"));
	SightSenseVisualization->SetupAttachment(RootComponent);
}

void ASeeker_Character::RotateAround(float RotationSpeed)
{
	FRotator RotationChangePerTick = RotateAroundDirection * RotationSpeed;
	RotatingMovementComponent->RotationRate = RotationChangePerTick;
}

void ASeeker_Character::StartRunning()
{
	if (auto CharacterMovementComponent = Cast<UCharacterMovementComponent>(GetMovementComponent()))
	{
		WalkSpeedCache = CharacterMovementComponent->MaxWalkSpeed;
		CharacterMovementComponent->MaxWalkSpeed = WalkSpeedCache * RunSpeedMultiplier;
	}
}

void ASeeker_Character::StopRunning()
{
	if (auto CharacterMovementComponent = Cast<UCharacterMovementComponent>(GetMovementComponent()))
	{
		CharacterMovementComponent->MaxWalkSpeed = WalkSpeedCache;
	}
}

void ASeeker_Character::BeginPlay()
{
	Super::BeginPlay();
	if (auto SeekerAIController = Cast<ASeeker_AIController>(GetController()))
	{
		BlackboardComponent = SeekerAIController->GetBlackboardComponent();
		if (auto AIPerceptionComponent = SeekerAIController->AIPerceptionComponent)
		{
			auto SenseID = AIPerceptionComponent->GetDominantSenseID();
			
			if (auto SenseConfig = Cast<UAISenseConfig_Sight>(AIPerceptionComponent->GetSenseConfig(SenseID)))
			{
				SightSenseVisualization->Radius = SenseConfig->SightRadius;
				SightSenseVisualization->Degree = SenseConfig->PeripheralVisionAngleDegrees * 2;
				SightSenseVisualization->CreateMesh();
				if (SightSenseVisualizationMaterial)
				{
					SightSenseVisualization->SetMaterial(0, SightSenseVisualizationMaterial);
				}
				else
				{
					UE_LOG(LogAIPerception, Error, TEXT("ERROR | ASeeker_AIController::SightSenseVisualizationMaterial | NULL Reference"));
				}
				
				return;
			}
		}
	}
	UE_LOG(LogAIPerception, Error, TEXT("ERROR | ASeeker_AIController::OnPossess | NULL Reference"));
}

void ASeeker_Character::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//Check If Stuck
	if (BlackboardComponent)
	{
		if (BlackboardComponent->GetValueAsBool("InMove") && GetVelocity().Length() <= 10.0f)
		{
			StuckTime += DeltaSeconds;
			if (StuckTime >= MaxStuckTime)
			{
				if (BlackboardComponent->GetValueAsObject("TargetPlayer") == false)
				{
					UnStuck();
				}
				StuckTime = 0.0f;
			}
		}
		else
		{
			StuckTime = 0.0f;
		}
	}
	else
	{
		UE_LOG(LogAIPerception, Error, TEXT("ERROR | ASeeker_Character::Tick | NULL Reference"));
	}
}

void ASeeker_Character::UnStuck()
{
	if (BlackboardComponent)
	{
		//Set TargetLocation = CurrentLocation, To Trigger T_GetNewTargetLocation
		BlackboardComponent->SetValueAsVector("TargetLocation", GetActorLocation());
	}
}
