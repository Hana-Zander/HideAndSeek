// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "Seeker_AIController.h"

#include "Seeker_Character.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BTNode.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AIPerceptionTypes.h"
#include "Perception/AISense_Sight.h"

ASeeker_AIController::ASeeker_AIController()
{
	AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerceptionComponent"));
	AIPerceptionComponent->SetDominantSense(UAISense_Sight::StaticClass()); 
}

void ASeeker_AIController::BeginPlay()
{
	Super::BeginPlay();
	if (!BehaviorTree)
	{
		UE_LOG(LogBehaviorTree, Error, TEXT("ERROR | ASeeker_AIController::BeginPlay | NO BEHAVIOR TREE FOUND"))
		return;
	}
	if (!RunBehaviorTree(BehaviorTree))
	{
		UE_LOG(LogBehaviorTree, Error, TEXT("ERROR | ASeeker_AIController::BeginPlay | RunBehaviorTree FAILED"))
		return;
	}
	AIPerceptionComponent->OnTargetPerceptionUpdated.AddUniqueDynamic(this, &ASeeker_AIController::OnPerceptionChanged);
}

bool ASeeker_AIController::InitializeBlackboard(UBlackboardComponent& BlackboardComp, UBlackboardData& BlackboardAsset)
{
	if (auto BlackboardOwner = BlackboardComp.GetOwner())
	{
		BlackboardComp.SetValueAsVector("TargetLocation", BlackboardOwner->GetActorLocation());	
	}
	BlackboardComp.SetValueAsFloat("RotateAroundTime", RotateAroundTime);
	
	return Super::InitializeBlackboard(BlackboardComp, BlackboardAsset);
}

//Because the AIPerceptionComponent::OnTargetPerceptionForgottenDelegate that was added in 5.2 doesn't get called like it should for unknown reasons,
//even when AIStimulus::Age >= AIStimulus::ExpirationAge is true
//an quick fix was used here to simulate the ForgetEffect of the AIPerceptionComponent with an Timer
void ASeeker_AIController::OnPerceptionChanged(AActor* Actor, FAIStimulus Stimulus)
{
	if (Stimulus.WasSuccessfullySensed())
	{
		if (Actor != GetBlackboardComponent()->GetValueAsObject("TargetPlayer"))
		{
			GetBlackboardComponent()->SetValueAsObject("TargetPlayer", Actor);
			OnTargetPlayerSensed();
		}
		GetWorld()->GetTimerManager().ClearTimer(ForgetTargetPlayerHandle);
		ForgetTargetPlayerHandle.Invalidate();
	}
	else
	{
		GetWorld()->GetTimerManager().SetTimer(ForgetTargetPlayerHandle, this, &ASeeker_AIController::OnTargetPlayerForgotten, RememberPlayerTime);
	}
}

void ASeeker_AIController::OnTargetPlayerSensed()
{
	//Abort Signal is used to bring Seeker out of wait-task
	GetBlackboardComponent()->SetValueAsBool("AbortSignal", true);
	GetBlackboardComponent()->SetValueAsBool("TargetLocationReached", false);
	GetBlackboardComponent()->SetValueAsBool("InMove", false);

	if (auto SeekerCharacter = Cast<ASeeker_Character>(GetPawn()))
	{
		SeekerCharacter->StartRunning();
	}
}

void ASeeker_AIController::OnTargetPlayerForgotten()
{
	GetBlackboardComponent()->SetValueAsObject("TargetPlayer", 0);
	GetBlackboardComponent()->SetValueAsBool("TargetLocationReached", false);
	GetBlackboardComponent()->SetValueAsBool("InMove", false);

	if (auto SeekerCharacter = Cast<ASeeker_Character>(GetPawn()))
	{
		SeekerCharacter->StopRunning();
	}
}

void ASeeker_AIController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	AIPerceptionComponent->OnTargetPerceptionUpdated.RemoveDynamic(this, &ASeeker_AIController::OnPerceptionChanged);
	Super::EndPlay(EndPlayReason);
}

void ASeeker_AIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

FGenericTeamId ASeeker_AIController::GetGenericTeamId() const
{
	return FGenericTeamId(0);
}
