// Copyright 2023 Martin/Hana Zander All rights reserved.


#include "BTT_SetBoolValue.h"

#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UBTT_SetBoolValue::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	OwnerComp.GetBlackboardComponent()->SetValueAsBool(BoolKey.SelectedKeyName, NewValue);
	
	return EBTNodeResult::Succeeded;
}
