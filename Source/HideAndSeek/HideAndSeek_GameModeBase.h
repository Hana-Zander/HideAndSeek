// Copyright 2023 Martin/Hana Zander All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Seeker_Character.h"
#include "GameFramework/GameModeBase.h"
#include "HideAndSeek_GameModeBase.generated.h"

class AHider_HUD;
class AHider_PlayerController;
class APlayerStart;

/**
 * 
 */
UCLASS()
class HIDEANDSEEK_API AHideAndSeek_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	TArray<APlayerStart*> HiderSpawnArray;
	TArray<APlayerStart*> SeekerSpawnArray;

	FTimerHandle HideTimerHandle;
	FTimerHandle SeekTimerHandle;

	UPROPERTY()
	AHider_PlayerController* HiderController = 0;

	UPROPERTY()
	AHider_HUD* HiderHUD = 0;

public:
	UPROPERTY(EditDefaultsOnly)
	FName HiderSpawnTag = "Hider";
	
	UPROPERTY(EditDefaultsOnly)
	FName SeekerSpawnTag = "Seeker";

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float HideTime = 30.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float SeekTime = 30.0f;

	UPROPERTY(EditDefaultsOnly)
	int SeekerAmount = 4;	
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASeeker_Character> SeekerCharacterClass;
	
private:
	void SetSpawnArrays();
	APlayerStart* GetRandomSpawn(TArray<APlayerStart*>& SpawnArray);
	void StartHideTimer();
	void StartSeekTimer();
	float GetTimerTimeLeft(const FTimerHandle& TimerHandle);
	void SpawnSeeker();

protected:
	virtual void OnHideTimerStart();
	virtual void OnSeekTimerStart();
	virtual void OnSeekTimerEnd();
	virtual void OnPlayerWin();
	virtual void OnPlayerLost();
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
public:
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual AActor* FindPlayerStart_Implementation(AController* Player, const FString& IncomingName) override;
	virtual void StartPlay() override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetHideTimeLeft();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetHideTimeLeftPercent();
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetSeekTimeLeft();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetSeekTimeLeftPercent();

	void EndGame(bool PlayerWon);

	UFUNCTION(BlueprintCallable)
	void RestartGame();
	
};
